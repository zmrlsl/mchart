/**
 * Created by zhoumingrui on 2017/2/5.
 */
'use strict';

var expect = require('chai').expect;
function add(a, b) {
    return a + b;
}
describe('测试add', function () {
    it('1+1=2', function () {
        expect(add(1, 1)).to.be.equal(2);
    });
});
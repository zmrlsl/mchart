/**
 * Created by zhoumingrui on 2017/2/1.
 */
class DarkTheme{
    static getInstance(){
        if(!this.instance){
            this.instance = new DarkTheme();
        }
        return this.instance;
    }
}

module.exports = DarkTheme;
/**
 * Created by zhoumingrui on 2017/2/1.
 */
class BasicTheme{
    static getInstance(){
        if(!this.instance){
            this.instance = new BasicTheme();
        }
        return this.instance;
    }
}

module.exports = BasicTheme;
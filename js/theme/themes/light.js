/**
 * Created by zhoumingrui on 2017/2/1.
 */
class LightTheme{
    static getInstance(){
        if(!this.instance){
            this.instance = new LightTheme();
        }
        return this.instance;
    }
}

module.exports = LightTheme;
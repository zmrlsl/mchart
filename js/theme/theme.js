/**
 * Created by zhoumingrui on 2017/2/1.
 */

const subclasses = {
    'basic': require('./themes/basic'),
    'light': require('./themes/light'),
    'dark': require('./themes/dark')
};

class Theme{

}

Theme.create = function (theme) {
    const ThemeClass = subclasses[theme.type] || Theme;
    return ThemeClass.getInstance();
};

module.exports = Theme;
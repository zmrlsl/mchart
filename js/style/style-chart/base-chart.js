/**
 * Created by zhoumingrui on 2017/1/30.
 */
const d3 = require('d3');

class BaseChart {

    _prepareData() {
    }

    render() {
    }

    _createDom(id) {
        id = id.indexOf('#') != -1 ? id : '#' + id;
        this.dom = d3.select(id);
    }

    _emptyDom() {
        document.getElementById(this.id).innerHTML = '';
    }

    get id() {
        return this.options.id;
    }

    set id(id) {
        this.options.id = id;
    }
}

module.exports = BaseChart;
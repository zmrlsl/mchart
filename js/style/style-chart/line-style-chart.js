/**
 * Created by zhoumingrui on 2017/1/25.
 */
const d3 = require('d3');
const BaseChart = require('./base-chart');
const padding = 10;

class LineStyleChart extends BaseChart {
    constructor(options) {
        this.options = Object.assign({}, {
            legend: 'LineChart',
            id: 'lineChart' + Math.random(),
            sourceId: null,
            source: null,
            dataType: null,
            xAxis: '',
            yAxis: '',
            gridShow: true,
            gridArea: true,
            container: null,
            area: null
        }, options);

        this._createDom(this.options.id);
        this._prepareFrame();

    }

    _prepareFrame() {
        /**
         * 清空
         */
        this._emptyDom();
        /**
         * 先准备数据
         */
        this._prepareData();
        /**
         * 准备XY轴
         */
        this._prepareAxis();
        /**
         * 绘制架子
         */

    }

    _prepareSize() {
        this.height = parseInt(document.getElementById(this.options.id).offsetHeight);
        this.width = parseInt(document.getElementById(this.options.id).offsetWidth);
    }

    _prepareData() {
        this._prepareSize();

        this.options.area = d3
            .area()
            .x((d, i) => {
                return (this.width - padding * 2) / 24 * i;
            })
            .y0((d, i) => {
                return this.height;
            })
            .y1((d, i) => {
                return this.height - this.y(d);
            });
    }

    _prepareAxis() {
        this.x = d3.scaleTime()
            .domain([new Date(2000, 0, 1), new Date(2000, 0, 2)])
            .range([0, this.width - padding * 2]);
        /*
         format 地址 https://github.com/d3/d3-time-format/blob/master/README.md#timeFormatLocale
         */
        var locale = d3.timeFormat("%H:%M");

        this.xAxis = d3.axisBottom(this.x)
            .ticks(24)
            .tickFormat(locale);

        this.dom.append("svg")
            .attr("width", this.width - padding * 2)
            .attr("height", this.height)
            .append("g")
            .attr("id", this.options.id + 'xAxis')
            .attr("transform", "translate(" + padding * 3 + "," + (this.height - padding * 2) + ")")
            .call(this.xAxis);

        this.y = d3.scaleLinear()
            .domain([0, 50])
            .range([this.height - padding * 3, 0]);

        this.yAxis = d3.axisLeft(this.y)
            .ticks(8);

        this.dom.select("svg")
            .append("g")
            .attr("id", this.options.id + 'yAxis')
            .attr("transform", "translate(" + (padding * 3) + "," + padding + ")")
            .call(this.yAxis);
    }

    resize(){
        this._prepareFrame();
    }

    render() {
        if (this.dom && this.options.source.updated) {

            let child = document.getElementById(this.options.id + "area");
            child && child.parentNode.removeChild(child);

            this.dom
                .select('svg')
                .attr('width', this.width - padding * 2)
                .attr('height', this.height)
                .append('g')
                .attr("id", this.options.id + "area")
                .attr("transform", "translate(" + (padding * 3 + 1) + "," + (-padding * 2) + ")")
                .append('path')
                .attr('d', this.options.area(this.options.source.data))
                .attr('fill', 'yellow');

            this.options.source.updated = false;
        }
    }

    _getMaxY() {
        return Math.max.apply(null, this.options.source.data);
    }
}

module.exports = LineStyleChart;
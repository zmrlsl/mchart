/**
 * Created by zhoumingrui on 2017/1/24.
 */
const assert = require('assert');

const mchart = module.exports = {};

mchart.version = require('../package.json').version;

mchart.alert = function () {
    alert('mchart 已经正常加载');
}

mchart.chart = require('./ui/mchart');

assert(mchart);


/**
 * Created by zhoumingrui on 2017/1/30.
 */
class DataSource {
    /**
     * 数据源有id
     * @param options
     */
    constructor(options) {
        this.options = Object.assign({}, {
            id: '',
            data: null,
            type: ''
        }, options);
        this.updated = true;
    }

    formatData(formula) {
        this.options.data = this.options.data.map((d) => {
            return formula(d);
        })
    }

    get data() {
        return this.options.data;
    }

    set data(data) {
        this.options.data = data;
        this.updated = true;
    }

    get updated() {
        return this._dataUpdated;
    }

    set updated(updated) {
        this._dataUpdated = updated;
    }

    get id() {
        return this.options.id;
    }

    set id(id) {
        this.options.id = id;
    }
}
module.exports = DataSource;
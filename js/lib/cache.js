/**
 * Created by zhoumingrui on 2017/1/30.
 */
class ChartCache{
    set charts(charts){
        this._charts = charts;
    }
    get charts(){
        return this._charts;
    }
}

module.exports = ChartCache;
/**
 * Created by zhoumingrui on 2017/2/1.
 */

class Engine{
}

const subclasses = {
    'webgl': require('./webgl'),
    'svg': require('./svg'),
    'canvas': require('./canvas'),
};

Engine.create = function (chart) {
    const EngineClass = subclasses[chart.type] || Engine;
    return new EngineClass(chart);
};
module.exports = Engine;
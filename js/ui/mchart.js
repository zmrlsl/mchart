/**
 * Created by zhoumingrui on 2017/1/25.
 line    折线图，堆积折线图，区域图，堆积区域图。
 bar    柱形图（纵向），堆积柱形图，条形图（横向），堆积条形图。
 scatter    散点图，气泡图。散点图至少需要横纵两个数据，更高维度数据加入时可以映射为颜色或大小，当映射到大小时则为气泡图
 k    K线图，蜡烛图。常用于展现股票交易数据。
 pie    饼图，圆环图。饼图支持两种（半径、面积）南丁格尔玫瑰图模式。
 radar    雷达图，填充雷达图。高维度数据展现的常用图表。
 chord    和弦图。常用于展现关系数据，外层为圆环图，可体现数据占比关系，内层为各个扇形间相互连接的弦，可体现关系数据
 force    力导布局图。常用于展现复杂关系网络聚类布局。
 map    地图。内置世界地图、中国及中国34个省市自治区地图数据、可通过标准GeoJson扩展地图类型。支持svg扩展类地图应用，如室内地图、运动场、物件构造等。
 heatmap    热力图。用于展现密度分布信息，支持与地图、百度地图插件联合使用。
 gauge    仪表盘。用于展现关键指标数据，常见于BI类系统。
 funnel    漏斗图。用于展现数据经过筛选、过滤等流程处理后发生的数据变化，常见于BI类系统。
 evnetRiver    事件河流图。常用于展示具有时间属性的多个事件，以及事件随时间的演化。
 treemap    矩形式树状结构图，简称：矩形树图。用于展示树形数据结构，优势是能最大限度展示节点的尺寸特征。
 venn    韦恩图。用于展示集合以及它们的交集。
 tree    树图。用于展示树形数据结构各节点的层级关系
 wordCloud    词云。词云是关键词的视觉化描述，用于汇总用户生成的标签或一个网站的文字内容
 */
const Event = require('../lib/event');
const Style = require('../style/style');

class Chart extends Event {
    constructor(options) {
        super();
        this.options = Object.assign({}, {
            style: {
                sources: [],
                charts: []
            }
        }, options);
        this.regist('style.doneloading', this.render);

        window.onresize = ()=>{
            this.resize();
        }

        this.repaintAble = true;
        this._style = new Style(this);
        this._parse();

    }

    /**
     * 解析style
     */
    _parse() {
        if (typeof this.options.style === 'object') {
            this._style.parseStyle();
        } else if (typeof this.options.style === 'string') {

        } else {
            this.repaintAble = false;
        }
    }

    /**
     * 渲染所有的图表
     */
    render() {
        this._style.renderStyle();
    }

    /**
     * 依据id渲染一个chart
     * @param id
     */
    renderChart(id) {
        this._style.renderStyle(id);
    }

    /**
     * 所有图表重绘
     */
    repaint() {
        if (!this.repaintAble) return;

        this.render();
    }

    /**
     * 窗口重置
     */
    resize(){
        this._style.resize();
    }

    /**
     * 更新对应id的source
     * @param id
     */
    updateSource(id, data) {
        this._style.updateSource(id, data);
    }
}

module.exports = Chart;